# 1、下载手机软件
在手机上需要下载IP摄像头软件，可以通过内置RTSP和HTTP服务器把手机设备变成包含双向音频支持的无线IP摄像头并用于安全监控，你可以使用电脑上的浏览器查看，在这里我用OpenCV读取视频流并进行录屏操作<br />![image.png](https://cdn.nlark.com/yuque/0/2022/png/26141959/1645789372664-f8413d9c-3606-4374-97c0-ff21b1de22b7.png#clientId=ueb52389d-5ff2-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=259&id=uc3ca48e7&margin=%5Bobject%20Object%5D&name=image.png&originHeight=517&originWidth=1005&originalType=binary&ratio=1&rotation=0&showTitle=false&size=132752&status=done&style=none&taskId=ua6681019-e25e-40df-9a90-43f5e527bd3&title=&width=502.5)
# 2、获取视频流地址
打开下好的软件，点击“打开IP摄像头服务器”,可以获得在当前内网下，视频流的地址，在这里我选择第一个RTSP协议的视频流，访问时需要输入账号密码，默认都为admin<br />![image.png](https://cdn.nlark.com/yuque/0/2022/png/26141959/1645789587772-d6fe16c1-86f2-48ac-9216-2e85e020f7f2.png#clientId=ueb52389d-5ff2-4&crop=0&crop=0&crop=1&crop=0.7878&from=paste&height=584&id=ufa612f81&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1792&originWidth=828&originalType=binary&ratio=1&rotation=0&showTitle=false&size=613798&status=done&style=none&taskId=u4e143105-abaa-4897-93f2-56521cd49ae&title=&width=270)
# 3、代码编写
## 3.1 读取视频流
新建VideoCapture对象，读取视频流
```python
cap = cv2.VideoCapture("rtsp://admin:admin@******.local:8554/live")
```
## 3.2 显示视频
用while语句循环读取视频，并显示出来
```python
while True:
    success,img = cap.read()
    cv2.imshow("camera",img)
```
## 3.3 录制视频
新建VideoWriter对象，将图像写入视频，需要指定几个参数

1. 表示读取的视频帧所存放的新的文件 
1. 指的是视频存放的编码格式 
3. 表示每秒的帧数 
3. 表示图像的长宽大小
```python
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
size = (int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH)), \
        int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT)))
out = cv2.VideoWriter("name.mp4", fourcc, 25, self.size)
out.write(img)    #将图片写入视频
```
