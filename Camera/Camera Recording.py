import cv2 
import time
class Camera:
    def __init__(self, addr, save_dir="./"):
        """initial value"""
        self.save_dir = save_dir
        self.addr = addr
        self.isRecoding = False
        self.capture =cv2.VideoCapture(self.addr)
        self.fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        self.size = (int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH)), int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        
    def exit(self):
        """exit"""
        self.capture.release()
        if self.isRecoding:
            self.out.release()
        cv2.destroyWindow("camera")
        print("exit")

    def mark(self,img):
        now = int(time.time())
        timeArray = time.localtime(now)
        if self.isRecoding:
            state = "Recoding:"
        else:
            state = "Normal:"
        otherStyleTime = state + time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
        otherStyleTime = otherStyleTime.encode("gbk").decode(errors="ignore")
        cv2.putText(img, otherStyleTime, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (55,255,155), 2)
        return img

    def open(self):
        """open camera"""
        cv2.namedWindow("camera",1)
        while (self.capture.isOpened()):
            
            success,img = self.capture.read()
            if success:
                cv2.namedWindow("camera",0)
                key = cv2.waitKey(5)
                if key == 32:       #按空格键开始录制和结束录制
                    self.isRecoding = not self.isRecoding
                    print("isRecoding",self.isRecoding)
                    if self.isRecoding:
                        vedeo_name = time.strftime("%Y%m%d%H%M%S", time.localtime(int(time.time()))) + ".mp4"
                        self.out = cv2.VideoWriter(self.save_dir+vedeo_name, self.fourcc, 25, self.size)
                        print()
                    else:
                        self.out.release()
                img = self.mark(img)
                result = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
                if self.isRecoding:
                    self.out.write(result)
                cv2.imshow("camera",img)
                if key == 27:       #ESC键退出
                    self.exit()
            else:
                print("Fail to get image")
if __name__ == '__main__':
    addr = "rtsp://admin:admin@zzttekiiPhone.local:8554/live"
    cam1 = Camera(addr)
    cv2.namedWindow("camera",1)
    cam1.open()





