# -*- coding: utf-8 -*-

import re
import multiprocessing
import time
import requests


def task(novel_index):
    res = requests.get('http://www.ranwenw.com/0_1'+novel_index)
    res.encoding = 'gbk'
    res = res.text
    temp = res.replace('\n','').replace('&nbsp',' ').replace('<br />','').replace('<br />','\r').replace(';','')
    content = re.findall('<div id="content">(.+)</div>\r\t\t\t\t<s',temp)
    name = re.findall('class="bookname">\r\t\t\t\t\t<h1>(.*)</h1>',temp)
    print('正在爬取：{}'.format(name))
    with open(name[0].replace('?',' ')+'.txt','w') as f:
            f.writelines(content)
    time.sleep(2)

def main(num):
    url = 'http://www.ranwenw.com/0_1/'
    res = requests.get(url)
    res.encoding = 'gbk'
    html = res.text
    novel_index = re.findall('<dd><a href="/0_1(.*)">第',html)[:1001]
    pool = multiprocessing.Pool(processes=num)
    print("创建成功")
    output_list = pool.map(task,[i for i in novel_index])


if __name__ == "__main__":
    t1 = time.time()
    main(10)
    t2 = time.time()
    print("多进程时间{}".format(t2-t1))